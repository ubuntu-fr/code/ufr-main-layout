# Gabarit principal pour le site ubuntu-fr.org

> :warning: **ABANDONNÉ** :
> 
> Les assets généraux sont maintenant inclus directement à [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms).
> - [main.css](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/blob/master/static/css/main.css)
> - [print.css](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/blob/master/static/css/print.css)
> - [app.js](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/blob/master/static/js/app.js)
> - [cookie.js](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/blob/master/static/js/cookie.js)
> - [fonts](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/tree/master/static/fonts)
> - [images](https://gitlab.com/ubuntu-fr/code/ufr-cms/-/tree/master/static/img)

## features

basé (plus ou moins) sur les guidelines officielles de canonical : [design.ubuntu.com](https://design.ubuntu.com/)  
et inspiré du [travail de Cytheria](https://ubuntu.dahwa.fr)

- RESPONSIVE !
- 2 thèmes : light et dark
- **CSS pur** (pas de préprocesseur : sass, less, postcss, etc.)
- **HTML5**
- **JavaScript ES6** pur - avec modules (pas de préprocesseur : WebPack, TypeScript, etc.)

## guide

Template à intégrer sur l'ensemble du site : *CMS*, *doc* (wiki), *forum*.

Le contenu de `<main>` est un exemple. Pour chaque page il faut aussi ajuster `<title>`, les meta `description` et `og:image` dans `<head>`.

Le CSS `main.css` doit prendre en compte le contenu éditorial pour toutes les plateformes, ce qui allège le travail et permet de s'assurer d'un contenu unifié (et éventuellement mutualise le poids des assets).
Un minimum de CSS doit être ajouté dans des fichiers spécifiques à chaque plateforme (styles très spécifiques ou styles des éléments d'interface principalement - exemple donné avec `doc.css`).

Thème sombre : ajout de la classe `.dark` sur le tag `<html>` (paramètre lié à un cookie).

## demo

[ubuntu-fr.gitlab.io/code/ufr-main-layout/](https://ubuntu-fr.gitlab.io/code/ufr-main-layout/)  
(pages Accueil, Documentation et Forum)
