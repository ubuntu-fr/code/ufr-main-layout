function hideControls() {
  document.querySelector('#control-panel').classList.remove('opened')
}

document.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#handle').addEventListener('click', function() {
    document.querySelector('#control-panel').classList.toggle('opened')
  })
  document.querySelectorAll('#control-panel a').forEach(function(item) {
    item.addEventListener('click', function() {
      hideControls()
    })
  })
  document.querySelector('main > .dw-content').addEventListener('click', function() {
    hideControls()
  })
})
