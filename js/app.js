import { getCookie, setCookie } from './cookie.js'

let cookieTheme = getCookie('theme')

function changeTheme(theme) {
  if (theme == 'light') {
    document.querySelector('#switcher-light').style.display = 'none'
    document.querySelector('#switcher-dark').style.display = 'block'
    document.documentElement.classList.remove('dark')
    setCookie('theme','light')
  }
  else {
    document.querySelector('#switcher-dark').style.display = 'none'
    document.querySelector('#switcher-light').style.display = 'block'
    document.documentElement.classList.add('dark')
    setCookie('theme','dark')
  }
}
if (cookieTheme) changeTheme(cookieTheme)
else changeTheme('light')

function hideMenu() {
  document.querySelector('body > header').classList.remove('opened')
}

document.addEventListener('DOMContentLoaded', function() {
  document.querySelector('#switcher-light').addEventListener('click', function() {
    changeTheme('light')
  })
  document.querySelector('#switcher-dark').addEventListener('click', function() {
    changeTheme('dark')
  })
  document.querySelector('#sandwich').addEventListener('click', function() {
    document.querySelector('body > header').classList.toggle('opened')
  })
  document.querySelectorAll('header .switcher, header nav a').forEach(function(item) {
    item.addEventListener('click', function() {
      hideMenu()
    })
  })
  document.querySelector('#switcher-wide').addEventListener('click', function() {
    document.documentElement.classList.toggle('wide')
  })
})
